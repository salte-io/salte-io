# https://salte.io

[![Travis][travis-ci-image]][travis-ci-url]
[![Coveralls][coveralls-image]][coveralls-url]

[![Commitizen friendly][commitizen-image]][commitizen-url]
[![Greenkeeper badge][greenkeeper-image]][greenkeeper-url]

Salte's Marketing Site

## Quick Start

```sh
# Install the Node Modules
$ yarn
# Start the server!
$ yarn start serve
```

[travis-ci-image]: https://img.shields.io/travis/salte-io/salte.io/master.svg?style=flat
[travis-ci-url]: https://travis-ci.org/salte-io/salte.io

[coveralls-image]: https://img.shields.io/coveralls/salte-io/salte.io/master.svg
[coveralls-url]: https://coveralls.io/github/salte-io/salte.io?branch=master

[commitizen-image]: https://img.shields.io/badge/commitizen-friendly-brightgreen.svg
[commitizen-url]: https://commitizen.github.io/cz-cli/

[greenkeeper-image]: https://badges.greenkeeper.io/salte-io/salte.io.svg
[greenkeeper-url]: https://greenkeeper.io
