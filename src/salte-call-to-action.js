import {PolymerElement, html} from '@polymer/polymer/polymer-element.js';

class SalteCallToAction extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          overflow: hidden;
          color: white;
          margin: 20px 0;
          padding: 45px 0;
        }

        .content {
          display: block;
          position: relative;
          margin: 0;
          max-width: initial;
          overflow: hidden;
          padding: 20px;
          color: white;
          box-sizing: border-box;
          transform: rotate(3deg) translateX(-1%);
          width: 102%;
          text-align: center;
          font-weight: bold;
          text-decoration: none;
          backface-visibility: hidden;

          background: #212121;
          transition: 0.15s background-color ease-in-out;
        }

        .content[href]:hover {
          background: #3B3B3B;
        }

        .content > ::slotted(strong) {
          color: pink;
        }
      </style>

      <a href$="[[href]]" target$="[[target]]" class="content">
        <slot></slot>
      </a>
    `;
  }

  static get properties() {
    return {
      href: String,
      target: String
    };
  }
}

customElements.define('salte-call-to-action', SalteCallToAction);
