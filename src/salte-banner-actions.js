import {PolymerElement, html} from '@polymer/polymer/polymer-element.js';

class SalteBannerActions extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: flex;
          flex-direction: row;
          font-size: 16px;
          margin-bottom: 20px;
        }

        :host > ::slotted(*) {
          margin: 5px;
        }

        @media only screen and (max-width: 500px) {
          :host {
            flex-direction: column;
          }
        }
      </style>
      <slot></slot>
    `;
  }
}

customElements.define('salte-banner-actions', SalteBannerActions);
