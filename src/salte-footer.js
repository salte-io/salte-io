import {PolymerElement, html} from '@polymer/polymer/polymer-element.js';

import './salte-footer-section.js';

class SalteFooter extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          margin-top: 50px;
          background: #212121;
          color: white;
        }

        :host > ::slotted(.split) {
          border-bottom: 1px solid rgba(255, 255, 255, 0.3);
        }
      </style>
      <slot></slot>
    `;
  }
}

customElements.define('salte-footer', SalteFooter);
