import '@webcomponents/webcomponentsjs/webcomponents-sd-ce.js';

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-pages/iron-pages.js';

import page from 'page';

import './polyfill/promise.prototype.finally.js';

import Raven from 'raven-js';

import { version } from '../package.json';

Raven.config('https://138be25022e74eedbc41be43fcb4e295@sentry.io/301482', {
  whitelistUrls: [
    /https?:\/\/(www\.)?salte\.io/,
    /https?:\/\/alpha\.salte\.io/,
    /https?:\/\/salte-live\.firebaseapp\.com/,
    /https?:\/\/salte-alpha\.firebaseapp\.com/
  ],
  release: version
}).install();

import './salte-banner.js';
import './salte-banner-actions.js';
import './salte-button.js';
import './salte-footer.js';

import './page/salte-home.js';
import './page/salte-platforms.js';
import './page/salte-about.js';
import './page/salte-careers.js';
import './page/salte-admin.js';
import './page/salte-404.js';
// These are legal pages
import './page/salte-terms.js';
import './page/salte-privacy.js';

class SalteApp extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: flex;
          flex-direction: column;
          flex: 1;
        }

        #pages {
          display: flex;
          flex-direction: column;
          flex: 1;
        }

        .social > a {
          display: inline-flex;
          align-items: center;
          justify-content: center;
          height: 30px;
          width: 30px;
          font-size: 18px;
          color: white;

          transition: 0.15s color ease-in-out;
        }

        .social > a:hover {
          color: lightgray;
        }
      </style>
      <salte-banner>
        <salte-banner-actions>
          <salte-button href="/" active="[[_page(page, 'home')]]">
            <svg xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns="http://www.w3.org/2000/svg" height="24" width="24" version="1.1" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/" viewBox="0 0 24 24">
              <path d="m12 4a8 8 0 0 0 -8 8 8 8 0 0 0 8 8 8 8 0 0 0 8 -8 8 8 0 0 0 -8 -8zm-2.2793 2.9609a1.32 1.32 0 0 1 1.3183 1.3184 1.32 1.32 0 0 1 -1.3183 1.3203 1.32 1.32 0 0 1 -1.3203 -1.3203 1.32 1.32 0 0 1 1.3203 -1.3184zm4.5583 0a1.32 1.32 0 0 1 1.321 1.3184 1.32 1.32 0 0 1 -1.321 1.3203 1.32 1.32 0 0 1 -1.318 -1.3203 1.32 1.32 0 0 1 1.318 -1.3184zm-6.5993 3.7191a1.32 1.32 0 0 1 1.3203 1.32 1.32 1.32 0 0 1 -1.3203 1.32 1.32 1.32 0 0 1 -1.3203 -1.32 1.32 1.32 0 0 1 1.3203 -1.32zm4.3203 0a1.32 1.32 0 0 1 1.32 1.32 1.32 1.32 0 0 1 -1.32 1.32 1.32 1.32 0 0 1 -1.32 -1.32 1.32 1.32 0 0 1 1.32 -1.32zm4.32 0a1.32 1.32 0 0 1 1.321 1.32 1.32 1.32 0 0 1 -1.321 1.32 1.32 1.32 0 0 1 -1.32 -1.32 1.32 1.32 0 0 1 1.32 -1.32zm-6.5993 3.72a1.32 1.32 0 0 1 1.3183 1.321 1.32 1.32 0 0 1 -1.3183 1.318 1.32 1.32 0 0 1 -1.3203 -1.318 1.32 1.32 0 0 1 1.3203 -1.321zm4.5583 0a1.32 1.32 0 0 1 1.321 1.321 1.32 1.32 0 0 1 -1.321 1.318 1.32 1.32 0 0 1 -1.318 -1.318 1.32 1.32 0 0 1 1.318 -1.321z"/>
              <path d="m12 0a12 12 0 0 0 -12 12 12 12 0 0 0 12 12 12 12 0 0 0 12 -12 12 12 0 0 0 -12 -12zm0 2.4a9.6 9.6 0 0 1 9.6 9.6 9.6 9.6 0 0 1 -9.6 9.6 9.6 9.6 0 0 1 -9.6 -9.6 9.6 9.6 0 0 1 9.6 -9.6z"/>
            </svg>
          </salte-button>
          <salte-button href="/platforms" active="[[_page(page, 'platforms')]]">
            Platforms
          </salte-button>
          <salte-button href="/about" active="[[_page(page, 'about')]]">
            About Us
          </salte-button>
        </salte-banner-actions>
        <div id="header" class="header">[[header]]</div>
        <div id="content">[[description]]</div>
      </salte-banner>
      <iron-pages id="pages"
        selected="{{page}}"
        attr-for-selected="page"
        fallback-selection="404">
        <salte-home page="home"></salte-home>
        <salte-platforms page="platforms"></salte-platforms>
        <salte-about page="about"></salte-about>
        <salte-careers page="careers"></salte-careers>
        <salte-admin page="admin"></salte-admin>
        <salte-404 page="404"></salte-404>
        <!-- These are legal pages -->
        <salte-terms page="terms"></salte-terms>
        <salte-privacy page="privacy"></salte-privacy>
      </iron-pages>
      <salte-footer>
        <salte-footer-section class="social">
          <salte-button href="https://twitter.com/SalteTech" target="_blank">
            Twitter
          </salte-button>
          <salte-button href="https://www.facebook.com/salte.io" target="_blank">
            Facebook
          </salte-button>
          <salte-button href="#" target="_blank">
            YouTube
          </salte-button>
          <salte-button href="https://www.linkedin.com/company/salte" target="_blank">
            LinkedIn
          </salte-button>
        </salte-footer-section>
        <div class="split"></div>
        <salte-footer-section>
          <salte-button href="/terms">Terms</salte-button>
          <salte-button href="/privacy">Privacy</salte-button>
          <div class="split"></div>
          <div class="dimmed">v[[version]]</div>
        </salte-footer-section>
      </salte-footer>
    `;
  }

  static get properties() {
    return {
      version: {
        type: String,
        value: version,
        reflectToAttribute: true
      }
    };
  }

  connectedCallback() {
    super.connectedCallback();
    window.Salte = window.Salte || {};
    if (Salte.app) {
      this.parentElement.removeChild(this);
      throw new Error('Whoops, looks like the app was registered twice!');
    }
    Salte.app = this;

    page('/:page?', (context) => {
      const page = context.params.page || 'home';
      if (page === this.page) return;

      const selectedPage = this.$.pages.selectedItem;

      const promises = [];
      if (selectedPage) {
        promises.push(Promise.resolve(selectedPage.hide && selectedPage.hide(true)));
        promises.push(this.$.header.animate([
          { opacity: 1 },
          { opacity: 0 }
        ], {
          duration: 1000,
          easing: 'cubic-bezier(0.4, 0, 0.2, 1)',
          fill: 'forwards'
        }).finished.then((animation) => {
          this.$.header.style.visibility = 'hidden';
          animation.cancel();
        }));
        promises.push(this.$.content.animate([
          { opacity: 1 },
          { opacity: 0 }
        ], {
          duration: 1000,
          easing: 'cubic-bezier(0.4, 0, 0.2, 1)',
          fill: 'forwards'
        }).finished.then((animation) => {
          this.$.content.style.visibility = 'hidden';
          animation.cancel();
        }));
      } else {
        this.$.header.style.visibility = 'hidden';
        this.$.content.style.visibility = 'hidden';
      }

      Promise.all(promises).then(() => {
        const previousPage = this.page;
        this.set('page', page);
        const selectedPage = this.$.pages.selectedItem;

        const promises = [];

        this.removeAttribute('unresolved');

        if (selectedPage) {
          this.setProperties({
            header: selectedPage.header,
            description: selectedPage.description
          });

          this.$.header.style.visibility = '';
          this.$.content.style.visibility = '';
          promises.push(selectedPage.show && selectedPage.show(!!previousPage));
          promises.push(this.$.header.animate([
            { opacity: 0 },
            { opacity: 1 }
          ], {
            duration: 1000,
            easing: 'cubic-bezier(0.4, 0, 0.2, 1)',
            fill: 'forwards'
          }));
          promises.push(this.$.content.animate([
            { opacity: 0 },
            { opacity: 1 }
          ], {
            duration: 1000,
            easing: 'cubic-bezier(0.4, 0, 0.2, 1)',
            fill: 'forwards'
          }));
        }

        return Promise.all(promises);
      }).catch((error) => {
        console.error(error);
        this.set('page', '404');
      });
    });
    page();
  }

  _page(page, expectedPage) {
    return page === expectedPage;
  }
}

customElements.define('salte-app', SalteApp);
