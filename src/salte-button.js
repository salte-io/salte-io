import {PolymerElement, html} from '@polymer/polymer/polymer-element.js';

class SalteButton extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }

        a {
          display: flex;
          align-items: center;
          justify-content: center;
          min-height: 38px;
          padding: 0 20px;
          text-decoration: none;
          border-radius: 3px;
          cursor: pointer;

          background: rgba(255, 255, 255, 0.1);
          color: white;
          fill: white;
          transition: 0.15s ease-in-out;
          transition-property: background-color, color, fill;
        }

        :host([active]) a {
          pointer-events: none;

          background: rgba(255, 255, 255, 0.2);
        }

        :host([disabled]) a {
          pointer-events: none;

          color: rgba(255, 255, 255, 0.3);
          fill: rgba(255, 255, 255, 0.3);
        }

        a:hover {
          background: rgba(255, 255, 255, 0.3);
        }

        a, a:active, a:visited, a:hover {
          text-decoration: inherit;
        }

        @media only screen and (max-width: 500px) {
          :host {
            flex-direction: column;
          }
        }
      </style>
      <a href$="[[href]]" target$="[[target]]" rel="noopener">
        <slot></slot>
      </a>
    `;
  }

  static get properties() {
    return {
      href: String,
      target: String,

      active: {
        type: Boolean,
        value: false,
        reflectToAttribute: true
      },

      disabled: {
        type: Boolean,
        value: false,
        reflectToAttribute: true
      }
    };
  }
}

customElements.define('salte-button', SalteButton);
