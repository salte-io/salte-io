import {PolymerElement, html} from '@polymer/polymer/polymer-element.js';

class SalteOverlay extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: flex;
          align-items: center;
          justify-content: center;
          position: absolute;
          top: 0;
          left: 0;
          right: 0;
          bottom: 0;
          color: rgba(255, 255, 255, 0.5);
          background: rgba(0, 0, 0, 0.5);
          font-weight: 900;
          font-size: 80px;

          pointer-events: none;
          opacity: 0;
          transition: 0.15s opacity ease-in-out;
        }

        :host([opened]) {
          pointer-events: all;
          opacity: 1;
        }

        :host([disabled]) {
          pointer-events: none;
        }
      </style>
      <slot></slot>
    `;
  }

  static get properties() {
    return {
      opened: {
        type: Boolean,
        value: false,
        reflectToAttribute: true
      },

      disabled: {
        type: Boolean,
        value: false,
        reflectToAttribute: true
      }
    };
  }
}

customElements.define('salte-overlay', SalteOverlay);
