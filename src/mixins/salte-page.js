import 'web-animations-js/web-animations-next-lite.min.js';

export default function(superClass) {
  return class extends superClass {
    show(animate) {
      this.style.display = '';
      if (animate) {
        return this.animate([
          { transform: 'translateY(-20px)', opacity: 0 },
          { transform: 'translateY(0)', opacity: 1 }
        ], {
          duration: 1000,
          easing: 'cubic-bezier(0.4, 0, 0.2, 1)'
        }).finished;
      }
    }

    hide(animate) {
      if (animate) {
        return this.animate([
          { transform: 'translateY(0)', opacity: 1 },
          { transform: 'translateY(-20px)', opacity: 0 }
        ], {
          duration: 1000,
          easing: 'cubic-bezier(0.4, 0, 0.2, 1)',
          fill: 'forwards'
        }).finished.then((animation) => {
          this.style.display = 'none';
          animation.cancel();
        });
      }

      this.style.display = 'none';
    }

    get header() {
      return 'Salte';
    }

    get description() {
      return 'Cloud solutions built for tomorrows consumers today!';
    }
  };
}
