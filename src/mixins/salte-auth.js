import SalteAuth from '@salte-io/salte-auth';

import config from '../salte-config.js';

// Configure SalteAuth with Auth0's url and client id.
const auth = new SalteAuth({
  providerUrl: config.providerUrl,
  responseType: 'id_token',
  redirectUrl: location.origin,
  clientId: config.clientId,
  scope: 'openid',

  provider: 'auth0'
});

const AuthMixin = function(superClass) {
  return class extends superClass {
    static get properties() {
      return {
        user: {
          type: Object,
          value: null
        },

        groups: {
          type: Boolean,
          computed: '_computeGroups(user)'
        },

        authenticated: {
          type: Boolean,
          computed: '_computeAuthenticated(user)'
        }
      };
    }

    connectedCallback() {
      super.connectedCallback();
      // Set the User on startup
      this.set('user', auth.profile.userInfo);

      auth.on('login', (error, user) => {
        if (error) {
          console.error(error);
          return;
        }

        this.set('user', user);
      });

      auth.on('logout', (error) => {
        if (error) {
          console.error(error);
          return;
        }

        this.set('user', null);
      });
    }

    contains(list, item) {
      return list.includes(item);
    }

    _computeGroups(user) {
      if (!user) {
        return null;
      }

      return user['http://salte.io/groups'] || [];
    }

    _computeAuthenticated(user) {
      return !!user;
    }
  };
};

export { auth, AuthMixin };
export default AuthMixin;
