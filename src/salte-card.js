import {PolymerElement, html} from '@polymer/polymer/polymer-element.js';

import './salte-overlay.js';

class SalteCard extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          overflow: hidden;
          border-radius: 3px;
          position: relative;

          background: white;
          box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.2);
          transition: 0.15s ease-in-out;
          transition-property: background-color, box-shadow, color;
        }

        :host([href]:hover) {
          box-shadow: 0px 2px 10px 0px rgba(0, 0, 0, 0.2);
          background: #EEEEEE;
        }

        :host([disabled]) {
          pointer-events: none;
        }

        :host([disabled]) #disabled {
          opacity: 1;
        }

        #content ::slotted(.header) {
          font-size: 20px;
          font-weight: bold;
        }

        #content {
          display: grid;
          grid-gap: 20px;
          padding: 20px;
        }

        #image {
          height: 200px;
          background-size: cover;
        }

        a {
          display: block;
          color: inherit;
          text-decoration: inherit;
        }
      </style>
      <a href$="[[href]]" target$="[[target]]" rel="noopener">
        <div id="image"></div>
        <div id="content">
          <slot></slot>
        </div>
        <salte-overlay opened="[[disabled]]" disabled>
          <slot name="disabled"></slot>
        </salte-overlay>
      </a>
    `;
  }

  static get properties() {
    return {
      href: {
        type: String,
        reflectToAttribute: true
      },

      target: String,

      imageUrl: {
        type: String,
        observer: '_imageUrlChanged'
      },

      disabled: {
        type: Boolean,
        value: false,
        reflectToAttribute: true
      }
    };
  }

  _imageUrlChanged(imageUrl) {
    if (imageUrl) {
      this.$.image.style.backgroundImage = `url('${imageUrl}')`;
    } else {
      this.$.image.style.display = 'none';
    }
  }
}

customElements.define('salte-card', SalteCard);
