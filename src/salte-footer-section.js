import {PolymerElement, html} from '@polymer/polymer/polymer-element.js';

class SalteFooterSection extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: flex;
          flex-direction: row;
          align-items: center;
          max-width: 1200px;
          margin: auto;
          padding: 20px 10px;
        }

        :host > ::slotted(.split) {
          display: flex;
          flex: 1;
        }

        :host > ::slotted(*) {
          margin: 5px;
        }

        :host > ::slotted(.dimmed) {
          display: flex;
          align-items: center;
          justify-content: center;
          margin: 0 10px;
          min-height: 38px;
          color: rgba(255, 255, 255, 0.3);
        }

        :host > ::slotted(a) {
          display: flex;
          align-items: center;
          justify-content: center;
          text-decoration: inherit;
          margin: 0 10px;
          color: rgba(255, 255, 255, 0.3);
        }

        :host > ::slotted(a:hover),
        :host > ::slotted(a:focus) {
          text-decoration: underline;
        }

        @media only screen and (max-width: 500px) {
          :host {
            flex-direction: column;
            align-items: normal;
          }

          :host > ::slotted(*) {
            flex: 1;
          }

          :host > ::slotted(.split) {
            display: none;
          }
        }
      </style>
      <slot></slot>
    `;
  }
}

customElements.define('salte-footer-section', SalteFooterSection);
