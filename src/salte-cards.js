import {PolymerElement, html} from '@polymer/polymer/polymer-element.js';

import './salte-card.js';

class SalteCards extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: grid;
          grid-template-columns: repeat(3, 1fr);
          grid-gap: 20px;
          grid-auto-rows: minmax(100px, auto);
        }

        /* Smartphones (landscape) ----------- */
        @media all and (min-width: 481px) and (max-width : 770px) {
          :host {
            grid-template-columns: repeat(2, 1fr);
          }
        }

        /* Smartphones (portrait) ----------- */
        @media all and (max-width : 480px) {
          :host {
            grid-template-columns: repeat(1, 1fr);
          }
        }
      </style>
      <slot></slot>
    `;
  }
}

customElements.define('salte-cards', SalteCards);
