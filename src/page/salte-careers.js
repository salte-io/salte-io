import {PolymerElement, html} from '@polymer/polymer/polymer-element.js';

import PageMixin from '../mixins/salte-page.js';

class SalteCareers extends PageMixin(PolymerElement) {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }

        .content {
          max-width: 1200px;
          margin: auto;
          padding: 0 20px;
        }

        h1 {
          font-size: 40px;
          font-weight: bold;
          text-align: center;
        }
      </style>
      <div class="content">TODO: Careers</div>
    `;
  }

  get header() {
    return 'Careers';
  }

  get description() {
    return `Join our team in building a better tomorrow!`;
  }
}

customElements.define('salte-careers', SalteCareers);
