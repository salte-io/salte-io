import {PolymerElement, html} from '@polymer/polymer/polymer-element.js';

import PageMixin from '../mixins/salte-page.js';

class SalteAbout extends PageMixin(PolymerElement) {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }

        .content {
          max-width: 1200px;
          margin: auto;
          padding: 0 20px;
        }

        h1 {
          font-size: 40px;
          font-weight: bold;
          text-align: center;
        }
      </style>
      <div class="content">TODO: About Us</div>
    `;
  }

  get header() {
    return 'About Us';
  }

  get description() {
    return `Learn what we're all about!`;
  }
}

customElements.define('salte-about', SalteAbout);
