import {PolymerElement, html} from '@polymer/polymer/polymer-element.js';

import PageMixin from '../mixins/salte-page.js';

class Salte404 extends PageMixin(PolymerElement) {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }

        .content {
          max-width: 1200px;
          margin: auto;
          padding: 0 20px;
        }

        h1 {
          font-size: 40px;
          font-weight: bold;
          text-align: center;
        }
      </style>
      <h1 class="content">Four Oh Four</h1>
    `;
  }
}

customElements.define('salte-404', Salte404);
