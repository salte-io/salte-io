import {PolymerElement, html} from '@polymer/polymer/polymer-element.js';

import PageMixin from '../mixins/salte-page.js';

import '../salte-call-to-action.js';
import '../salte-cards.js';

class SaltePlatforms extends PageMixin(PolymerElement) {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }

        .content {
          max-width: 1200px;
          margin: auto;
          padding: 0 20px;
        }
      </style>

      <salte-call-to-action>
        Applications built for <strong>you</strong>!
      </salte-call-to-action>

      <div class="content">
        <salte-cards>
          <template is="dom-repeat" items="[[applicationsForYou]]" as="application">
            <salte-card
              image-url="[[application.imageUrl]]"
              href="[[application.url]]" target="_blank"
              disabled="[[application.disabled]]">
              <div class="header">[[application.name]]</div>
              <div>[[application.description]]</div>
            <div slot="disabled">WIP</div>
            </salte-card>
          </template>
        </salte-cards>
      </div>

      <salte-call-to-action>
        Applications built for <strong>developers</strong> by <strong>developers</strong>!
      </salte-call-to-action>

      <div class="content cards">
        <salte-cards>
          <template is="dom-repeat" items="[[applicationsForDevelopers]]" as="application">
            <salte-card
              image-url="[[application.imageUrl]]"
              href="[[application.url]]" target="_blank"
              disabled="[[application.disabled]]">
              <div class="header">[[application.name]]</div>
              <div>[[application.description]]</div>
            <div slot="disabled">WIP</div>
            </salte-card>
          </template>
        </salte-cards>
      </div>

      <salte-call-to-action>
        Libraries built for the <strong>community</strong>!
      </salte-call-to-action>

      <div class="content">TODO: Invoke GitHub / NPM APIs</div>
    `;
  }

  static get properties() {
    return {
      applicationsForYou: {
        type: Array,
        value: function() {
          return [{
            name: 'Quillie',
            description: 'An app designed to simplify sharing and collaborating on lists.',
            imageUrl: require('../../assets/quillie.png'),
            url: 'https://quillie.net'
          }];
        }
      },

      applicationsForDevelopers: {
        type: Array,
        value: function() {
          return [{
            name: `Let's Preview!`,
            description: 'An app designed for developers by developers to simplify the act of reviewing changes to your front-end code!',
            imageUrl: require('../../assets/lets-preview.png'),
            url: 'https://letspreview.io',
            disabled: true
          }];
        }
      }
    };
  }

  get header() {
    return 'Platforms';
  }

  get description() {
    return `Here are the platforms we've developed thus far!`;
  }
}

customElements.define('salte-platforms', SaltePlatforms);
