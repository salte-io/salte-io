import {PolymerElement, html} from '@polymer/polymer/polymer-element.js';

import {auth, AuthMixin} from '../mixins/salte-auth.js';
import PageMixin from '../mixins/salte-page.js';

class SalteAdmin extends AuthMixin(PageMixin(PolymerElement)) {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }

        .content {
          max-width: 1200px;
          margin: auto;
          padding: 0 20px;
        }

        h1 {
          font-size: 40px;
          font-weight: bold;
          text-align: center;
        }
      </style>
      <div class="content">
        <template is="dom-if" if="[[authenticated]]">
          <p>Looks like you've logged in!</p>
          <button on-click="logout">Logout</button>
          <template is="dom-if" if="[[contains(groups, 'salte-admin')]]">
            <p>Looks like you have access!</p>
          </template>
          <template is="dom-if" if="[[!contains(groups, 'salte-admin')]]">
            <p>Looks like you don't have access!</p>
          </template>
        </template>
        <template is="dom-if" if="[[!authenticated]]">
          <p>Looks like you need to log in!</p>
          <button on-click="login">Login</button>
        </template>
      </div>
    `;
  }

  get header() {
    return 'Admin';
  }

  get description() {
    return `Settings for salte.io`;
  }

  login() {
    this.set('loading', true);
    auth.loginWithNewTab().finally(() => {
      this.set('loading', false);
    });
  }

  logout() {
    this.set('loading', true);
    auth.logoutWithIframe().finally(() => {
      this.set('loading', false);
    });
  }
}

customElements.define('salte-admin', SalteAdmin);
