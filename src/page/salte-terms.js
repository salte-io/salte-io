import {PolymerElement, html} from '@polymer/polymer/polymer-element.js';

import PageMixin from '../mixins/salte-page.js';

import '../salte-call-to-action.js';

class SalteTerms extends PageMixin(PolymerElement) {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }

        .content {
          max-width: 1200px;
          margin: auto;
          padding: 0 20px;
        }

        h1 {
          font-size: 40px;
          font-weight: bold;
          text-align: center;
        }
      </style>
      <div class="content">
        <h1>Terms of Service</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vestibulum massa a orci vulputate posuere. Nam non sagittis ante. In eu hendrerit nisl, ut placerat purus. Maecenas et elit vitae quam malesuada porttitor quis eget libero. Aliquam blandit tellus nisi, eu laoreet nunc lacinia dignissim. Pellentesque vel congue quam. Maecenas porttitor velit ante, eu porttitor est hendrerit rutrum. Fusce ac fermentum nisi, non ornare lacus. Integer dictum lacus sit amet nunc suscipit porta. Sed ornare purus in libero vulputate rhoncus.</p>
        <p>Maecenas eget urna tortor. Suspendisse elementum ante tortor, vitae pharetra velit ultrices imperdiet. Vivamus eu mauris magna. Proin laoreet feugiat vulputate. Donec aliquet viverra nisi, in mollis neque. Phasellus cursus gravida nibh eu blandit. Morbi laoreet nisl ac lacus sollicitudin suscipit. Curabitur rhoncus dui a mi efficitur tempus. Maecenas condimentum tincidunt lectus, et pharetra mauris facilisis ac. Curabitur nec tellus eget libero pharetra suscipit. Integer varius lacinia tempor.</p>
      </div>
    `;
  }
}

customElements.define('salte-terms', SalteTerms);
