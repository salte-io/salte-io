import {PolymerElement, html} from '@polymer/polymer/polymer-element.js';

class SalteBanner extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          padding: 20px;
          overflow: hidden;
          color: white;
          font-size: 30px;
          margin-bottom: -100px;
        }

        .flashy {
          display: block;
          background: rebeccapurple;
          background: linear-gradient(8deg, pink, rebeccapurple);
          box-shadow: inset 0px -5px 10px 0px rgba(0, 0, 0, 0.2);
          backface-visibility: hidden;

          transform: rotate(3deg) translate(-58px, -110px);
          width: 100%;
          padding: 50px 50px 120px 50px;
        }

        .content {
          max-width: 1200px;
          margin: auto;

          transform: rotate(-3deg) translateY(55px);
        }

        .content > ::slotted(.header) {
          font-size: 60px;
          margin-bottom: 20px;
        }
      </style>
      <div class="flashy">
        <div class="content">
          <slot></slot>
        </div>
      </div>
    `;
  }
}

customElements.define('salte-banner', SalteBanner);
